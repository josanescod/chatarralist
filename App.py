import os
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
from werkzeug import secure_filename
from flask_weasyprint import HTML, render_pdf

app = Flask(__name__)

# conexion a la bdd
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'admin'
app.config['MYSQL_PASSWORD'] = 'b1u2r381'
app.config['MYSQL_DB'] = 'flask_inventario'
mysql = MySQL(app)

# configuracion directorio guardar ficheros
app.config['UPLOAD_FOLDER'] = 'static/images/'
# settings
app.secret_key = 'mysecretkey'


@app.route('/')
def Index():
    
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM hardware')
    data = cur.fetchall()
    cur.execute('SELECT SUM(precio) FROM hardware')
    precioTotal = cur.fetchall()
    print(data)
    print(precioTotal[0])
    return render_template('index.html', hardware=data, precio=precioTotal[0])


@app.route('/add', methods=['POST'])
def add():
    if request.method == 'POST':
        # datos del producto
        imagen = request.files['imagen']
        nombre = request.form['nombre']
        modelo = request.form['modelo']
        unidades = request.form['unidades']
        precio = request.form['precio']

        precionum = float(precio)
        unidadesnum = float(unidades)
        totalprecionum = precionum*unidadesnum
        precioActualizado = str(totalprecionum)
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO hardware (imagen,nombre,modelo,unidades,precio) VALUES (%s,%s,%s,%s,%s)',
                    (imagen.filename, nombre, modelo, unidades, precioActualizado))
        mysql.connection.commit()
        # guardando imagen del producto
        try:
            imagen.save(os.path.join(
                app.config['UPLOAD_FOLDER'], secure_filename(imagen.filename)))
        except:
            print('no hay imagen')
        print(imagen.filename)
        print('imagen cargada exitosamente')
        flash('Elemento añadido exitosamente')
        return redirect(url_for('Index'))


@app.route('/delete/<string:id>')
def delete(id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM hardware WHERE id = {0}'.format(id))
    mysql.connection.commit()
    flash('Elemento borrado exitosamente')
    return redirect(url_for('Index'))


@app.route('/edit/<id>')
def get_contact(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM hardware WHERE id = %s', [id])
    data = cur.fetchall()
    return render_template('edit-hardware.html', dato=data[0])


@app.route('/update/<id>', methods=['POST'])
def update_contact(id):
    if request.method == 'POST':
        imagen = request.files['imagen']
        if imagen.filename == '':
            nombre = request.form['nombre']
            modelo = request.form['modelo']
            unidades = request.form['unidades']
            precio = request.form['precio']
            precionum = float(precio)
            unidadesnum = float(unidades)
            totalprecionum = precionum*unidadesnum
            precioActualizado = str(totalprecionum)
            cur = mysql.connection.cursor()
            cur.execute('''
                            UPDATE hardware
                             SET 
                                 nombre = %s,
                                 modelo = %s,
                                 unidades = %s,
                                 precio = %s
                             WHERE id = %s        
                                ''', (nombre, modelo, unidades, precioActualizado, id))
            mysql.connection.commit()
        else:
            
            # guardando imagen del producto
            imagen.save(os.path.join(
                app.config['UPLOAD_FOLDER'], secure_filename(imagen.filename)))
            nombre = request.form['nombre']
            modelo = request.form['modelo']
            unidades = request.form['unidades']
            precio = request.form['precio']
            precionum = float(precio)
            unidadesnum = float(unidades)
            totalprecionum = precionum*unidadesnum
            precioActualizado = str(totalprecionum)
            # primero eliminar la imagen que hay en la bdd anterior
            cur = mysql.connection.cursor()
            cur.execute('SELECT imagen FROM hardware WHERE id = %s', [id])
            nombreAnterior = cur.fetchall()
            print('fichero a eliminar:')
            print(nombreAnterior[0][0])
            try:
                os.remove(os.path.join(
                    app.config['UPLOAD_FOLDER'], nombreAnterior[0][0]))
            except:
                print('no se ha podido localizar la imagen')
            cur.execute('''
                            UPDATE hardware
                             SET 
                                 imagen = %s, 
                                 nombre = %s,
                                 modelo = %s,
                                 unidades = %s,
                                 precio = %s
                             WHERE id = %s        
                                ''', (imagen.filename, nombre, modelo, unidades, precioActualizado, id))
            mysql.connection.commit()

        flash('Hardware actualizado satisfactoriamente')
        return redirect(url_for('Index'))


@app.route('/presupuesto')
def presupuesto():

    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM hardware')
    data = cur.fetchall()
    cur.execute('SELECT SUM(precio) FROM hardware')
    precioTotal = cur.fetchall()
    return render_template('presupuesto.html', hardware=data, precio=precioTotal[0])

@app.route('/info')
def info():
    return render_template('info.html')

@app.route('/pdf')
def pdf():

    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM hardware')
    data = cur.fetchall()
    cur.execute('SELECT SUM(precio) FROM hardware')
    precioTotal = cur.fetchall()
    print(data)
    print(precioTotal[0])
    html = render_template('presupuesto.html', hardware=data, precio=precioTotal[0])
    return render_pdf(HTML(string=html))


if __name__ == '__main__':
    # port = 7777 o el que se quiera quitando debug
    app.run(host='0.0.0.0', debug=True)
